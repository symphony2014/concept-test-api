﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ReturnMode; 

namespace ConceptApiController
{
    public class GroupController : BaseController
    {
        /// <summary>
        /// 添加更新座谈会组
        /// </summary>
        /// <param name="group"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn AddOrUpdateGroup(Group group)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                if (group.GroupId == 0)
                {
                    group.CreateDate = group.UpdateDate = DateTime.Now;
                    if (group.GroupName == null || group.GroupName == string.Empty || group.CardOrder == null || group.CardOrder == string.Empty)
                    {
                        vr.Result = 1;
                        vr.Message = "座谈会组名称或顺序不能为空";
                        return vr;
                    }
                    GroupBLL.AddItem(group);
                }
                else
                {
                    group.UpdateDate = DateTime.Now;
                    GroupBLL.UpdateItem(group, new string[] { "GroupName", "GroupPersonCount", "CardOrder", "UpdateDate" });
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
        /// <summary>
        /// 删除座谈会组
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public VoidReturn DelGroup(int groupId)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                bool delresult = GroupBLL.DelItemByGroupId(groupId);
                if (!delresult)
                {
                    vr.Result = 1;
                    vr.Message = "座谈会组不存在";
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }

        /// <summary>
        /// 获取座谈会组列表
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalListReturn<Group> GetGroupList(int projectAutoId)
        {
            NormalListReturn<Group> GroupList = new NormalListReturn<Group>()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                GroupList.data = GroupBLL.GetGroupList(projectAutoId);
                GroupList.TotalCount = GroupList.data.Count;
            }
            catch (Exception ex)
            {
                GroupList.Result = 2;
                GroupList.Message = "InternalError";
            }
            return GroupList;
        }
    }
}
