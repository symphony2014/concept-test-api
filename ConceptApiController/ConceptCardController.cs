﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using ConceptCommon.ReturnMode; 

namespace ConceptApiController
{
    public class ConceptCardController : BaseController
    {
        /// <summary>
        /// 添加更新概念卡
        /// </summary>
        /// <param name="project"></param>
        /// <returns></returns>
        [HttpPost]
        public VoidReturn AddOrUpdateCard(ConceptCard card)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                if (card.CardId == 0)
                {
                    card.CardOrder = ConceptCardBLL.GetOrderCardByProject(card.ProjectAutoId);
                    card.CreateDate = card.UpdateDate = DateTime.Now; 
                    if (card.ConceptCardName == null || card.ConceptCardName == string.Empty)
                    {
                        vr.Result = 1;
                        vr.Message = "概念卡名称不能为空";
                        return vr;
                    }
                    ConceptCardBLL.AddItem(card); 
                }
                else
                {
                    card.UpdateDate = DateTime.Now;
                    ConceptCardBLL.UpdateItem(card, new string[] { "ConceptCardName", "ConceptCardDesc"});
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        }
        /// <summary>
        /// 删除概念卡
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public VoidReturn DelConceptCard(int cardId)
        {
            VoidReturn vr = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            {
                bool delresult = ConceptCardBLL.DelItemByCardId(cardId);
                if (!delresult)
                {
                    vr.Result = 1;
                    vr.Message = "概念卡不存在";
                }

            }
            catch (Exception ex)
            {
                vr.Result = 2;
                vr.Message = "InternalError";
            }
            return vr;
        } 
        
        /// <summary>
        /// 上传概念卡图片
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <param name="cardId"></param>
        /// <returns></returns>
        public VoidReturn UploadCardImage(int projectAutoId, int cardId)
        {
            if (HttpContext.Current.Request.HttpMethod.Equals("OPTIONS"))
            {
                return null;
            }
            VoidReturn message = new VoidReturn()
            {
                Result = 0,
                Message = "成功"
            };
            try
            { 
                HttpPostedFile file = HttpContext.Current.Request.Files[0];
                if ((file.ContentType != "image/gif") && (file.ContentType != "image/jpeg") && (file.ContentType != "image/png"))
                {
                    message.Result = 1;
                    message.Message = "不是图片格式";
                    return message;
                }
                else if (file.ContentLength >= Convert.ToInt32(ConfigurationManager.AppSettings["PerCardSize"]))
                {
                    message.Result = 1;
                    message.Message = "图片大小超限，请重新上传图片";
                    return message;
                }
                else
                { 
                    var card = ConceptCardBLL.GetConceptCardByCardId(cardId); 
                    if(card!=null)
                    {
                        int thisFileSize = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(file.ContentLength / 1024)));
                        #region 检查该项目的概念卡大小总和
                        var listCard = ConceptCardBLL.GetConceptCardList(projectAutoId); 
                        int totalSize = thisFileSize + listCard.Where(c => c.FileSize > 0 && c.CardId != cardId).ToList().Sum(c => c.FileSize);
                        if(totalSize>=Convert.ToInt32(ConfigurationManager.AppSettings["TotalCardSize"]))
                        {
                            message.Result = 1;
                            message.Message = "该项目所有概念卡图片大小超限，请重新上传该图片。";
                            return message;
                        }
                        #endregion

                        int d = file.FileName.LastIndexOf('.');
                        string extend = file.FileName.Substring((d + 1), file.FileName.Length - d - 1);
                        string fileName = cardId + "_" + DateTime.Now.ToString("yyyyMMddHHmmss_ffff", DateTimeFormatInfo.InvariantInfo) + "." + extend;
                        string imagePath = ConfigurationManager.AppSettings["UploadCardImage"];
                        string UploadUrl = imagePath + projectAutoId + "\\";
                        card.FileSize = thisFileSize;
                        if (!Directory.Exists(UploadUrl))
                        {
                            Directory.CreateDirectory(UploadUrl);
                        }
                        string filePath = string.Concat(UploadUrl, fileName);
                        file.SaveAs(filePath);

                        string CardImageUrl = projectAutoId + "/" + fileName;
                        card.ImageUrl = CardImageUrl;
                        card.UpdateDate = DateTime.Now;
                        ConceptCardBLL.UpdateItem(card, new string[] { "ImageUrl", "UpdateDate", "FileSize" });
                    }
                    else
                    {
                        message.Result = 1;
                        message.Message = "概念卡不存在";
                        return message;
                    }

                    
                }

            }
            catch (Exception ex)
            {
                message.Result = 2;
                message.Message = ex.ToString();
            }
            HttpContext.Current.Response.Headers.Add("Content-Type", "application/json; charset=utf-8");
            return message;
        }

        /// <summary>
        /// 获取概念卡列表
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public NormalListReturn<ConceptCard> GetConceptCardList(int projectAutoId)
        {
            NormalListReturn<ConceptCard> ConceptCardList = new NormalListReturn<ConceptCard>()
            {
                Result = 0,
                Message = "成功"
            }; 
            try
            {
                var cardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
                if(cardList!=null)
                {
                    foreach(var card in cardList)
                    {
                        card.ImageUrl = ConfigurationManager.AppSettings["DisplayCardImage"] + card.ImageUrl;
                    }
                }
                ConceptCardList.data = cardList;
                ConceptCardList.TotalCount = ConceptCardList.data.Count;
            }
            catch (Exception ex)
            {
                ConceptCardList.Result = 2;
                ConceptCardList.Message = "InternalError";
            }
            return ConceptCardList;
        }
        /// <summary>
        /// 获取默认的概念卡顺序按创建时间正序排列
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        public string GetConceptCardDefaultOrder(int projectAutoId)
        {
            string result = string.Empty;
            var cardList = ConceptCardBLL.GetConceptCardList(projectAutoId);
            StringBuilder sb = new StringBuilder();
            if (cardList != null)
            {
                foreach (var card in cardList)
                {
                    sb.Append(card.CardOrder);
                    sb.Append(",");
                }
            }
            result = sb.ToString().TrimEnd(',');
            return result;
        }
    }
}
