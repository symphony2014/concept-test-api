﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Concept.BLL.BaseBD;
using Concept.Entity.BaseDB;
using Concept.Entity.SysMode;
using ConceptApiController.BLL;
using ConceptCommon;
using ConceptCommon.ReturnMode; 

namespace ConceptApiController
{
    public class ReportController: BaseController
    {
        /// <summary>
        /// 打分表情包报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public GradingReportReturn GetGradingIconReport(int projectAutoId)
        {
            GradingReportReturn result = new GradingReportReturn();
            try
            { 
                return  ReportLogical.Instance.GetGradingEmotionDataNew(projectAutoId); 
            }
            catch (Exception ex)
            {
                return null;
            }

            return result;
        }
        
        /// <summary>
        /// 排序报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public OrderReportReturn GetOrderReport(int projectAutoId)
        {
            OrderReportReturn result = new OrderReportReturn();
            try
            { 
                return  ReportLogical.Instance.GetOrderDataNew(projectAutoId); 
            }
            catch (Exception ex)
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// 分堆报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public ClassifyReportReturn GetClassifyReport(int projectAutoId)
        {
            ClassifyReportReturn result = new ClassifyReportReturn();
            try
            {
                return ReportLogical.Instance.GetClassifyDataNew(projectAutoId); 
            }
            catch (Exception ex)
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// 涂鸦报告
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <returns></returns>
        [HttpGet]
        public PaintingReportReturn GetPaintingReport(int projectAutoId)
        {
            PaintingReportReturn result = new PaintingReportReturn();
            try
            {
                result.header = new List<string>(); 
                result.header.Add("组名");
                result.header.Add("设备ID");
                result.header.Add("概念卡"); 

                List<Group> groupList = GroupBLL.GetGroupList(projectAutoId); 
                List<DeviceAnswer> AllAnswerList = DeviceAnswerBLL.GetDeviceAnswerListByProject(projectAutoId).OrderBy(a => a.GroupId).ThenBy(a => a.DeviceId).ToList();
                List<ConceptCard> conceptCardList = ConceptCardBLL.GetConceptCardList(projectAutoId); 
                
                if (groupList != null && groupList.Count > 0)
                {
                    result.groups = new List<PaintingGroup>();
                    foreach (var groupItem in groupList)
                    {

                        PaintingGroup paintingGroup = new PaintingGroup();
                        paintingGroup.name = groupItem.GroupName;
                        paintingGroup.device = new List<PaintingDeviceInfo>();
                        var groupByDeviceAnswers = AllAnswerList.Where(a => a.GroupId == groupItem.GroupId && a.QuestionType == Concept.Entity.EnumMode.DeviceAnswerQuestionTypeEnum.Painting).GroupBy(pet => pet.DeviceId).Select(g => (new { DeviceId = g.Key, Answers = g.ToList() }));
                        foreach (var device in groupByDeviceAnswers)
                        {
                            PaintingDeviceInfo paintingDeviceInfoItem = new PaintingDeviceInfo();
                            paintingDeviceInfoItem.deviceId = device.DeviceId;
                            paintingDeviceInfoItem.cards = new List<PaintingCard>();

                             
                            if(conceptCardList!=null && conceptCardList.Count>0)
                            {
                                foreach(var card in conceptCardList)
                                {
                                   PaintingCard paintingCardItem = new PaintingCard();
                                   var thisCardAnswer = device.Answers.Where(a => a.CardId == card.CardId).FirstOrDefault();
                                   if(thisCardAnswer!=null)
                                   {
                                       string DrawingimagePath = ConfigurationManager.AppSettings["DisplayPaintingCardImage"] + thisCardAnswer.QuestionAnswer; 
                                       paintingCardItem.name = "C" + card.CardOrder;
                                       paintingCardItem.url = DrawingimagePath;
                                       paintingCardItem.detailName = card.ConceptCardName;
                                       paintingDeviceInfoItem.cards.Add(paintingCardItem);
                                   }
                                }
                            }
                            paintingGroup.device.Add(paintingDeviceInfoItem);
                        }
                        result.groups.Add(paintingGroup);
                    }
                } 
            }
            catch (Exception ex)
            {
                return null;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="projectAutoId"></param>
        /// <param name="reportType"></param>
        [HttpGet]
        public void DownLoadData(int projectAutoId, int reportType)
        {
            List<List<string>> dataList = new List<List<string>>();
            switch (reportType)
            {
                case 1: dataList = ReportLogical.Instance.GetGradingEmotionData(projectAutoId); break;
                case 2: dataList = ReportLogical.Instance.GetOrderData(projectAutoId); break;
                case 3: dataList = ReportLogical.Instance.GetClassifyData(projectAutoId); break;
                case 4: dataList = ReportLogical.Instance.GetPaintingData(projectAutoId); break;
                default:break;
            } 
            if(dataList.Count>0)
            {
                string rawDataPath = ConfigurationManager.AppSettings["DownloadData"];
                string filename = reportType + "_report.xlsx";
                if (!Directory.Exists(rawDataPath + projectAutoId + "\\"))
                {
                    Directory.CreateDirectory(rawDataPath + projectAutoId + "\\");
                }
                if (Directory.Exists(rawDataPath + projectAutoId + "\\" + filename))
                {
                    Directory.Delete(rawDataPath + projectAutoId + "\\" + filename);
                }
                DataSet ds = new DataSet();
                DataTable dt = ReportLogical.Instance.TransferToDT(dataList); 
                ds.Tables.Add(dt);
                CreateExcelFile.CreateExcelDocument(ds, rawDataPath + projectAutoId + "\\" + filename);
                HttpContext.Current.Response.Redirect(ConfigurationManager.AppSettings["DisplayDownloadData"] + projectAutoId + "/" + filename);
            }
        }
    }
}
