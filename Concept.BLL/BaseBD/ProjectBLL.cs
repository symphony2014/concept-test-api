﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class ProjectBLL: BLL<Project>
    {
        #region 定义
		private static object lockObject = new object();
        protected ProjectBLL()
            : base(new ProjectDAL())
        {
        }
        private static volatile ProjectBLL _instance;
        public static ProjectBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ProjectBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(Project item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(Project Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static bool DelItemByPjId(int PjId)
        {
            var item = Instance.Where("ProjectAutoId=@PjId").Parms(PjId).Get();
            if(item!=null)
            {
                return Instance.Del(item);
            }
            return false;
        }

        public static List<Project> GetProjectList(int page, int size, int ownerId, out int totalCount)
        {
            var list = new List<Project>();
            list = Instance.Where("OwnerId=@ownerId").Parms(ownerId).OrderBy("CreateDate desc").ListAll();
            totalCount = list.Count;
            if (size == -1)
            {
                return list;
            }
            return list.GetRange((page - 1) * size, (page * size) < totalCount ? size : totalCount - ((page - 1) * size));
        }

        public static Project GetProjectById(string pjid)
        {
            return Instance.Where("ProjectId=@pjid").Parms(pjid).Get();
        }
        public static Project GetProjectByAutoId(int pjid)
        {
            return Instance.Where("projectAutoId=@pjid").Parms(pjid).Get();
        }
        #endregion
    }
}
