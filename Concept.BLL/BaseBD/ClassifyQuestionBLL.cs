﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class ClassifyQuestionBLL: BLL<ClassifyQuestion>
    {
        #region 定义
		private static object lockObject = new object();
        protected ClassifyQuestionBLL()
            : base(new ClassifyQuestionDAL())
        {
        }
        private static volatile ClassifyQuestionBLL _instance;
        public static ClassifyQuestionBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new ClassifyQuestionBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static bool addItemList(List<ClassifyQuestion> Model)
        {
            return Instance.BatchAdd(Model);
        }     
        public static int UpdateItem(ClassifyQuestion Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static bool UpdateItemList(List<ClassifyQuestion> Model, string[] Cols = null)
        {
            return Instance.BatchUpdate(Model, Cols);
        }

        public static List<ClassifyQuestion> GetClassifyQuestionList(int projectAutoId)
        {
            var list = new List<ClassifyQuestion>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("ClassifyQuestionId").ListAll();
            return list;
        }
        #endregion
    }
}
