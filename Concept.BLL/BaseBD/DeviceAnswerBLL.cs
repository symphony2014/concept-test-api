﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class DeviceAnswerBLL: BLL<DeviceAnswer>
    {
        #region 定义
		private static object lockObject = new object();
        protected DeviceAnswerBLL()
            : base(new DeviceAnswerDAL())
        {
        }
        private static volatile DeviceAnswerBLL _instance;
        public static DeviceAnswerBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new DeviceAnswerBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(DeviceAnswer item)
        {
            return Transform.Int(Instance.Add(item));
        }

        public static int UpdateItem(DeviceAnswer Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }

        public static List<DeviceAnswer> GetDeviceAnswerListByProject(int projectAutoId)
        {
            var list = new List<DeviceAnswer>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).ListAll();
            return list;
        }
         
        #endregion
    }
}
