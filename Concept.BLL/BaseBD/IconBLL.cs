﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class IconBLL: BLL<Icon>
    {
        #region 定义
		private static object lockObject = new object();
        protected IconBLL()
            : base(new IconDAL())
        {
        }
        private static volatile IconBLL _instance;
        public static IconBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new IconBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static int AddItem(Icon item)
        {
            return Transform.Int(Instance.Add(item));
        }


        public static List<Icon> GetIconList()
        {
            var list = new List<Icon>();
            list = Instance.ListAll(); 
            return list;
        }
        #endregion
    }
}
