﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.BaseDB;
using Concept.Entity.BaseDB;
using FunLayer;
using ORM;

namespace Concept.BLL.BaseBD
{
    public class OrderQuestionBLL: BLL<OrderQuestion>
    {
        #region 定义
		private static object lockObject = new object();
        protected OrderQuestionBLL()
            : base(new OrderQuestionDAL())
        {
        }
        private static volatile OrderQuestionBLL _instance;
        public static OrderQuestionBLL Instance
        {
            get 
            {
                if (_instance == null)
                {
                    lock (lockObject)
                    {
                        return _instance ?? (_instance = new OrderQuestionBLL());
                    }
                }
                return _instance;
            }
        }
		#endregion

		#region 方法 
        public static bool addItemList(List<OrderQuestion> Model)
        {
            return Instance.BatchAdd(Model);
        }    
        public static int UpdateItem(OrderQuestion Model, string[] Cols = null)
        {
            return Transform.Int(Instance.Update(Model, Cols));
        }
        public static bool UpdateItemList(List<OrderQuestion> Model, string[] Cols = null)
        {
            return Instance.BatchUpdate(Model, Cols);
        }

        public static List<OrderQuestion> GetOrderQuestionList(int projectAutoId)
        {
            var list = new List<OrderQuestion>();
            list = Instance.Where("ProjectAutoId=@projectAutoId").Parms(projectAutoId).OrderBy("OrderQuestionId").ListAll();
            return list;
        }
        #endregion
    }
}
