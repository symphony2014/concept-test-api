﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.DAL.ConnectConfig;
using Concept.Entity.BaseDB;
using ORM;

namespace Concept.DAL.BaseDB
{
    public class AccountDAL: DAL<Account>
    {
        public AccountDAL()
        {
            initDb(ConceptDbconfig.GetNormal, ConceptDbconfig.GetRead, ConceptDbconfig.GetWrite);
        }
    }
}
