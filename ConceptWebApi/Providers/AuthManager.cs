﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.Entity.BaseDB;
using Concept.BLL.BaseBD;

namespace ConceptWebApi.Providers
{
    public class AuthManager
    {
        public AuthManager()
        {
        }
        public Task<Account> FindAsync(string userName, string password)
        {
            Task<Account> task = new Task<Account>(() =>
            {
                return AccountBLL.GetAccountByNamePassWord(userName, password);
            });
            task.Start();
            return task;
        }
    }
}