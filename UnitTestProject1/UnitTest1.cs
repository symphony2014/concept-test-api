﻿using System;
using ConceptApiController.SyncReport;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class SyncReportTest
    {
        [TestMethod]
        public  void SyncTest()
        {
            var sync = new Sync(60);
            var result= sync.SyncToReport();
            Assert.AreEqual(1,result.Status);
        }
    }
}
