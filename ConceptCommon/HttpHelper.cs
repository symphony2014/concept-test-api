﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;

namespace IpsosReport.Utility
{
    public class HttpHelper
    {
        private static CookieContainer cookie = new CookieContainer();

        public static string HttpPost(string Url, string postDataStr)
        {
            string retString = string.Empty;

            try
            {
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url);
                request.Method = "POST";
                request.ContentType = "application/json"; //"application/x-www-form-urlencoded";
                request.CookieContainer = cookie;
                byte[] Bodys = Encoding.UTF8.GetBytes(postDataStr);
                request.ContentLength = Bodys.Length;
                Stream myRequestStream = request.GetRequestStream();
                myRequestStream.Write(Bodys, 0, Bodys.Length);
                myRequestStream.Close();
                //StreamWriter myStreamWriter = new StreamWriter(myRequestStream, Encoding.GetEncoding("gb2312"));
                //myStreamWriter.Write(postDataStr);
                //myStreamWriter.Close();

                HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                response.Cookies = cookie.GetCookies(response.ResponseUri);
                Stream myResponseStream = response.GetResponseStream();
                StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
                retString = myStreamReader.ReadToEnd();
                myStreamReader.Close();
                myResponseStream.Close();
            }
            catch (Exception e)
            {
               // Log.WriteErrorLogInfo("HttpHelper.HttpPost(url+"+ Url + ",Data=["+ postDataStr + "]) 邮件发送失败！", e.Message);
                retString = "error";
            }

            return retString;
        }

        public static string HttpGet(string Url, string postDataStr)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(Url + (postDataStr == "" ? "" : "?") + postDataStr);
            request.Method = "GET";
            request.ContentType = "text/html;charset=UTF-8";

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            Stream myResponseStream = response.GetResponseStream();
            StreamReader myStreamReader = new StreamReader(myResponseStream, Encoding.GetEncoding("utf-8"));
            string retString = myStreamReader.ReadToEnd();
            myStreamReader.Close();
            myResponseStream.Close();

            return retString;
        }
    }
}
