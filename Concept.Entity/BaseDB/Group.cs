﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("Group")]
    public class Group
    {
        public Group() { }
        #region 字段属性
        protected int _groupId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("groupId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
        protected int _groupPersonCount = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("groupPersonCount", "INT")]
        public int GroupPersonCount
        {
            get { return _groupPersonCount; }
            set { _groupPersonCount = value; }
        }


        protected string _groupName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("groupName", "NVARCHAR", 50)]
        public string GroupName
        {
            get { return _groupName; }
            set { _groupName = value; }
        }

        protected string _cardOrder = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("cardOrder", "VARCHAR", 50)]
        public string CardOrder
        {
            get { return _cardOrder; }
            set { _cardOrder = value; }
        }
         
        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
        protected DateTime _updateDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("updateDate", "DATETIME")]
        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        } 
         
        #endregion
    }
}
