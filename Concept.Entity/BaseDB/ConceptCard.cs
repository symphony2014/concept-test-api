﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("ConceptCard")]
    public class ConceptCard
    {
        public ConceptCard() { }
        #region 字段属性
        protected int _cardId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("cardId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int CardId
        {
            get { return _cardId; }
            set { _cardId = value; }
        }
        protected int _projectAutoId = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("projectAutoId", "INT")]
        public int ProjectAutoId
        {
            get { return _projectAutoId; }
            set { _projectAutoId = value; }
        }
       
   
        protected int _cardOrder = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("cardOrder", "INT")]
        public int CardOrder
        {
            get { return _cardOrder; }
            set { _cardOrder = value; }
        }

        protected string _conceptCardName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("conceptCardName", "NVARCHAR", 50)]
        public string ConceptCardName
        {
            get { return _conceptCardName; }
            set { _conceptCardName = value; }
        }

        protected string _conceptCardDesc = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("conceptCardDesc", "NVARCHAR", 150)]
        public string ConceptCardDesc
        {
            get { return _conceptCardDesc; }
            set { _conceptCardDesc = value; }
        }

        protected string _imageUrl = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("ImageUrl", "VARCHAR", 250)]
        public string ImageUrl
        {
            get { return _imageUrl; }
            set { _imageUrl = value; }
        }

        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }

        protected DateTime _updateDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("updateDate", "DATETIME")]
        public DateTime UpdateDate
        {
            get { return _updateDate; }
            set { _updateDate = value; }
        }

        protected int _fileSize = 0;
        ///<summary>
        ///
        ///</summary>
        [DbField("fileSize", "INT")]
        public int FileSize
        {
            get { return _fileSize; }
            set { _fileSize = value; }
        }
        #endregion
    }
}
