﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM;

namespace Concept.Entity.BaseDB.Log
{
    [Serializable]
    [DbTable("Concept_OpMessage")]
    public class Concept_OpMessage
    {
        public Concept_OpMessage() { }			
		#region 字段属性
        protected int _id = 0;
		///<summary>
        ///
        ///</summary>
		[DbField("id", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }
        protected DateTime _addTime;
		///<summary>
        ///
        ///</summary>
		[DbField("addTime", "DATETIME")]
        public DateTime AddTime
        {
            get { return _addTime; }
            set { _addTime = value; }
        }
        protected DateTime _updateTime;
		///<summary>
        ///
        ///</summary>
		[DbField("updateTime", "DATETIME")]
        public DateTime UpdateTime
        {
            get { return _updateTime; }
            set { _updateTime = value; }
        }
        protected int _isDel = 0;
		///<summary>
        ///
        ///</summary>
		[DbField("isDel", "INT")]
        public int IsDel
        {
            get { return _isDel; }
            set { _isDel = value; }
        }
        
        protected string _errorOperation = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("errorOperation", "NVARCHAR", 200)]
        public string ErrorOperation
        {
            get { return _errorOperation; }
            set { _errorOperation = value; }
        }
        protected string _errorContext = string.Empty;
		///<summary>
        ///
        ///</summary>
		[DbField("errorContext", "NVARCHAR", -1)]
        public string ErrorContext
        {
            get { return _errorContext; }
            set { _errorContext = value; }
        }
        
        protected int _logExceptionId = 0;
		///<summary>
        ///
        ///</summary>
		[DbField("logExceptionId", "INT")]
        public int LogExceptionId
        {
            get { return _logExceptionId; }
            set { _logExceptionId = value; }
        }

        ///<summary>
        ///
        ///</summary>
        [DbField("msg", "NVARCHAR", 500)]
        public string Msg
        {
            get;
            set;
        }

		#endregion
    }
}
