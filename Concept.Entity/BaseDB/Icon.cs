﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("Icon")]
    public class Icon
    {
        public Icon() { }
        #region 字段属性
        protected int _iconId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("iconId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int IconId
        {
            get { return _iconId; }
            set { _iconId = value; }
        }


        protected string _iconUrl = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("iconUrl", "NVARCHAR", 50)]
        public string IconUrl
        {
            get { return _iconUrl; }
            set { _iconUrl = value; }
        }

         
        #endregion
    }
}
