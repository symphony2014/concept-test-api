﻿using System;
using System.Collections.Generic;
using ConceptCommon.ConvertMethod;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using ORM;

namespace Concept.Entity.BaseDB
{
    [DbTable("Account")]
    public class Account
    {
        public Account() { }
        #region 字段属性
        protected int _accountId = 0;
        ///<summary>
        ///
        ///</summary> 
        [DbField("accountId", "INT", ORMType = ORMFieldType.Identity | ORMFieldType.Key | ORMFieldType.Unique)]
        public int AccountId
        {
            get { return _accountId; }
            set { _accountId = value; }
        }
        protected string _loginName = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("loginName", "NVARCHAR", 50)]
        public string LoginName
        {
            get { return _loginName; }
            set { _loginName = value; }
        }
        protected string _password = string.Empty;
        ///<summary>
        ///
        ///</summary>
        [DbField("password", "VARCHAR", 50)]
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        } 

        protected DateTime _createDate = SqlDateTime.MinValue.Value;
        ///<summary>
        /// 
        ///</summary>
        [JsonConverter(typeof(DateTimeConverter))]
        [DbField("createDate", "DATETIME")]
        public DateTime CreateDate
        {
            get { return _createDate; }
            set { _createDate = value; }
        }
         
        #endregion
    }
}
