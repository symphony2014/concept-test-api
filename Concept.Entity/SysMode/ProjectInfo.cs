﻿using ConceptCommon.ConvertMethod;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    #region 项目信息
    public class ProjectInfo
    {
        /// <summary>
        /// 项目的自增长Id
        /// </summary>
        public int ProjectAutoId { get; set; }

        /// <summary>
        /// 项目Id
        /// </summary>
        public string ProjectId { get; set; }

        /// <summary>
        /// 项目名称
        /// </summary>
        public string ProjectName { get; set; }

        /// <summary>
        /// 项目的组
        /// </summary>
        public List<GroupInfo> Groups { get; set; }

        /// <summary>
        /// 项目的卡片
        /// </summary>
        public List<CardInfo> Cards { get; set; }

        /// <summary>
        /// 题信息
        /// </summary>
        public QuestionInfo Questions { get; set; }

        /// <summary>
        /// 资源文件包大小
        /// </summary>
        [JsonConverter(typeof(JsonLongConverter))]
        public long ResZipSize { get; set; }

        /// <summary>
        /// 资源文件的下载路径
        /// </summary>
        public string ResZipPath { get; set; }
/// <summary>
/// 模块展示顺序
/// </summary>
        public string ModuleOrder { get; set; }

        public int IsNeedDoodled { get; set; }
    }
    #endregion 项目信息

    #region 组信息
    /// <summary>
    /// 组信息
    /// </summary>
    public class GroupInfo
    {
        /// <summary>
        /// 组Id
        /// </summary>
        public int GroupId { get; set; }

        /// <summary>
        /// 组名
        /// </summary>
        public string GroupName { get; set; }

        /// <summary>
        /// 卡片出示顺序
        /// </summary>
        public string CardOrder { get; set; }
    }
    #endregion 组信息

    #region 卡片信息
    /// <summary>
    /// 卡片信息
    /// </summary>
    public class CardInfo
    {
        /// <summary>
        /// 卡片的序号
        /// </summary>
        public int CardOrder { get; set; }

        /// <summary>
        /// 卡片的Id
        /// </summary>
        public int CardId { get; set; }

        /// <summary>
        /// 卡片名字
        /// </summary>
        public string CardName { get; set; }

        /// <summary>
        /// 卡片图片的名称，带有图片后缀
        /// </summary>
        public string CardImageName { get; set; }
    

        public string CardImageUrl { get; set; }

    }
    #endregion 卡片信息

    #region 题信息
    /// <summary>
    /// 题信息
    /// </summary>
    public class QuestionInfo
    {
        /// <summary>
        /// 评分题信息
        /// </summary>
        public List<GradingQuestionInfo> GradingQuestions { get; set; }
        /// <summary>
        /// 涂鸦题类型
        /// </summary>
        public List<DoodledQuestionInfo> DoodledQuestions { get; set; }

        /// <summary>
        /// 开放题信息
        /// </summary>
        public OpenQuestionInfo OpenQuestion { get; set; }

        /// <summary>
        /// 表情包题信息
        /// </summary>
        public EmoticonQuestionInfo EmoticonQuestion { get; set; }

        /// <summary>
        /// 卡片分类题
        /// </summary>
        public List<ClassifyQuestionInfo> ClassifyQuestions { get; set; }

        /// <summary>
        /// 卡片排序题
        /// </summary>
        public List<OrderQuestionInfo> OrderQuestions { get; set; }
    }
    #endregion 题信息

    #region 题型基类
    /// <summary>
    /// 题型基类
    /// </summary>
    public class BasicQuestion
    {
        /// <summary>
        /// 题的类型
        /// </summary>
        public QuestionType QuestionType { get; set; }

        /// <summary>
        /// 题的label
        /// </summary>
        public string QuestionText { get; set; }
    }
    #endregion 题型基类


    #region Doodle题
    /// <summary>
    /// 涂鸦题
    /// </summary>
    public class DoodledQuestionInfo : BasicQuestion
    {
 
    }
    #endregion 评分题信息

    #region 评分题信息
    /// <summary>
    /// 评分题信息
    /// </summary>
    public class GradingQuestionInfo: BasicQuestion
    {
        /// <summary>
        /// 评分题Id
        /// </summary>
        public int QuestionId { get; set; }

        /// <summary>
        /// 评分题的出示顺序
        /// </summary>
        public int OrderId { get; set; }

    }
    #endregion 评分题信息

    #region 开放题信息
    /// <summary>
    /// 开放题信息
    /// </summary>
    public class OpenQuestionInfo : BasicQuestion
    { }
    #endregion 开放题信息

    #region 表情包题信息
    /// <summary>
    /// 表情包题信息
    /// </summary>
    public class EmoticonQuestionInfo : BasicQuestion
    {
        /// <summary>
        /// 选项
        /// </summary>
        public List<EmoticonQuestionOptionInfo> Options { get; set; }
    }
    #endregion 表情包题信息

    #region 表情包题的选项
    /// <summary>
    /// 表情包题的选项
    /// </summary>
    public class EmoticonQuestionOptionInfo
    {
        /// <summary>
        /// 选项Id
        /// </summary>
        public int OptionId { get; set; }

        /// <summary>
        /// 选项出示顺序
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// 选项分值
        /// </summary>
        public int Score { get; set; }

        /// <summary>
        /// 选项描述
        /// </summary>
        public string OptionText { get; set; }

        /// <summary>
        /// 选项表情图的名字
        /// </summary>
        public string IconName { get; set; }
    }
    #endregion 表情包题的选项

    #region 分类题
    /// <summary>
    /// 分类题
    /// </summary>
    public class ClassifyQuestionInfo: BasicQuestion
    {
        /// <summary>
        /// 类别维度出示顺序
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// 题型的 question id
        /// </summary>
        public int QuestionId { get; set; }

    }
    #endregion 分类题

    #region 排序题
    /// <summary>
    /// 排序题
    /// </summary>
    public class OrderQuestionInfo : BasicQuestion
    {
        /// <summary>
        /// 排序维度出示顺序
        /// </summary>
        public int OrderId { get; set; }

        /// <summary>
        /// 题的question id
        /// </summary>
        public int QuestionId { get; set; }
    }
    #endregion 排序题

}
