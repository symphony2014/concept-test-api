﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Concept.Entity.BaseDB;

namespace Concept.Entity.SysMode
{
    public class EmoticonSetting
    {
        public int ProjectAutoId { get; set; }
        public bool IsShowEmoticon { get; set; }
        public string EmoticonDesc { get; set; }
        public List<SysEmoticonQuestionOption> EmoticonQuestionOptionList { get; set; }
    }

    public class SysEmoticonQuestionOption
    {
        public EmoticonQuestionOptionFront EmoticonQuestionOptionItem { get; set; }
        public string EmoticonUrl { get; set; }
    }

    public class EmoticonQuestionOptionFront  
    {
        public int EmoticonOptionId {get;set;}
        public int ProjectAutoId { get; set; }
        public int OrderId { get; set; }
        public bool IsShow { get; set; }
        public int Score { get; set; }
        public int IconId { get; set; }
        public string IconDesc { get; set; } 
    }
}
