﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.SysMode
{
    /// <summary>
    /// 题目类型
    /// </summary>
    public enum QuestionType: int
    {
        Grading = 1, // 星星评分提
        OpenText = 2, // 开放题
        Icon = 3, // 表情题
        OrderQuestion = 4, // 排序题
        CategoryQuestion = 5, // 分类题
        Doodle=6//涂鸦题
    }
}
