﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Concept.Entity.EnumMode
{
    public enum DeviceAnswerQuestionTypeEnum:int
    {
        Default=0,
        Grading=1,
        OpenText =2,
        Icon =3,
        OrderQuestion = 4,
        CategoryQuestion = 5,
        Painting =6
    }
}
