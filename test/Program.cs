﻿using ConceptApiController.SyncReport;
using IpsosReport.ModelBiz.External;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace test
{
    class Program
    {
        static  void Main(string[] args)
        {
              Task<ReturnData> data=Sync();
            int x = 0;
            data.Wait();
            var result = data.Result;
        }
         
        static async Task<ReturnData> Sync()
        {
            var sync = new Sync(60);
            var result =await sync.SyncToReport();
            return result;
        }
    }
}
